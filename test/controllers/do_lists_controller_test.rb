require 'test_helper'

class DoListsControllerTest < ActionController::TestCase
  setup do
    @do_list = do_lists(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:do_lists)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create do_list" do
    assert_difference('DoList.count') do
      post :create, do_list: {  }
    end

    assert_redirected_to do_list_path(assigns(:do_list))
  end

  test "should show do_list" do
    get :show, id: @do_list
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @do_list
    assert_response :success
  end

  test "should update do_list" do
    patch :update, id: @do_list, do_list: {  }
    assert_redirected_to do_list_path(assigns(:do_list))
  end

  test "should destroy do_list" do
    assert_difference('DoList.count', -1) do
      delete :destroy, id: @do_list
    end

    assert_redirected_to do_lists_path
  end
end
