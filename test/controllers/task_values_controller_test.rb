require 'test_helper'

class TaskValuesControllerTest < ActionController::TestCase
  setup do
    @task_value = task_values(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:task_values)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create task_value" do
    assert_difference('TaskValue.count') do
      post :create, task_value: {  }
    end

    assert_redirected_to task_value_path(assigns(:task_value))
  end

  test "should show task_value" do
    get :show, id: @task_value
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @task_value
    assert_response :success
  end

  test "should update task_value" do
    patch :update, id: @task_value, task_value: {  }
    assert_redirected_to task_value_path(assigns(:task_value))
  end

  test "should destroy task_value" do
    assert_difference('TaskValue.count', -1) do
      delete :destroy, id: @task_value
    end

    assert_redirected_to task_values_path
  end
end
