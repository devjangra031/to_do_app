json.array!(@do_lists) do |do_list|
  json.extract! do_list, :id
  json.url do_list_url(do_list, format: :json)
end
