json.array!(@task_values) do |task_value|
  json.extract! task_value, :id
  json.url task_value_url(task_value, format: :json)
end
