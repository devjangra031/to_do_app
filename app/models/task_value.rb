class TaskValue
  include Mongoid::Document
  include Mongoid::Timestamps

  field :task_values, type: String
  field :task_values_publish, type: Boolean, :default => false

  belongs_to :do_lists
end
