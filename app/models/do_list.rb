class DoList
  include Mongoid::Document
  include Mongoid::Timestamps

  field :task, type: String
  # field :task_values, type: String
  field :task_publish, type: Boolean, :default => false

end
