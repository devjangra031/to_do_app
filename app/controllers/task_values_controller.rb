class TaskValuesController < ApplicationController
  before_action :set_task_value, only: [:show, :edit, :update, :destroy]

  # GET /task_values
  # GET /task_values.json
  def index
    if params[:task_id]
      task_id = params[:task_id]
      @task_values_true = TaskValue.where(:do_lists_id => task_id,:task_values_publish => true)
      @task_values_false = TaskValue.where(:do_lists_id => task_id,:task_values_publish => false)
    end
  end

  # GET /task_values/1
  # GET /task_values/1.json
  def show
  end

  # GET /task_values/new
  def new
    @task_value = TaskValue.new
  end

  # GET /task_values/1/edit
  def edit
  end

  # POST /task_values
  # POST /task_values.json
  def create
    @task_value = TaskValue.new(task_value_params)

    respond_to do |format|
      if @task_value.save
        format.html { redirect_to '/task_values?task_id='+task_value_params["do_lists_id"], notice: 'Task value was successfully created.' }
        format.json { render action: 'show', status: :created, location: @task_value }
      else
        format.html { render action: 'new' }
        format.json { render json: @task_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /task_values/1
  # PATCH/PUT /task_values/1.json
  def update
    respond_to do |format|
      if @task_value.update(task_value_params)
        format.html { redirect_to '/task_values?task_id='+task_value_params["do_lists_id"], notice: 'Task value was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @task_value.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /task_values/1
  # DELETE /task_values/1.json
  def destroy
    do_lists_id = @task_value.do_lists_id
    @task_value.destroy
    respond_to do |format|
      format.html { redirect_to '/task_values?task_id='+do_lists_id }
      format.json { head :no_content }
    end
  end

  def task_values_publish_mark
    if params[:task_id]
      @output = TaskValue.where(:id => params[:value_id])
      if @output
        @output.update(:task_values_publish => true)
      end
      redirect_to '/task_values?task_id='+params[:task_id]
    end
  end

  def task_values_publish_unmark
    if params[:task_id]
      @output = TaskValue.where(:id => params[:value_id])
      if @output
        @output.update(:task_values_publish => false)
      end
      redirect_to '/task_values?task_id='+params[:task_id]
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_task_value
      @task_value = TaskValue.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_value_params
      params.require(:task_value).permit(:task_values,:do_lists_id,:task_values_publish)
    end
end
