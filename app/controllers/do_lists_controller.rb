class DoListsController < ApplicationController
  before_action :set_do_list, only: [:show, :edit, :update, :destroy]

  # GET /do_lists
  # GET /do_lists.json
  def index
    # @do_lists = DoList.all
    @do_lists_true = DoList.where(:task_publish => true)
    @do_lists_false = DoList.where(:task_publish => false)
  end

  # GET /do_lists/1
  # GET /do_lists/1.json
  def show
  end

  # GET /do_lists/new
  def new
    @do_list = DoList.new
  end

  # GET /do_lists/1/edit
  def edit
  end

  # POST /do_lists
  # POST /do_lists.json
  def create
    @do_list = DoList.new(do_list_params)

    # puts "---------#{do_list_params}"
    # do_list_params.split(',').each do |x|
    #   TaskValues.create(:task_values => x)
    # end

    respond_to do |format|
      if @do_list.save
        format.html { redirect_to do_lists_url, notice: 'Do list was successfully created.' }
        format.json { render action: 'show', status: :created, location: @do_list }
      else
        format.html { render action: 'new' }
        format.json { render json: @do_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /do_lists/1
  # PATCH/PUT /do_lists/1.json
  def update
    respond_to do |format|
      if @do_list.update(do_list_params)
        format.html { redirect_to do_lists_url, notice: 'Do list was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @do_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /do_lists/1
  # DELETE /do_lists/1.json
  def destroy
    @do_list.destroy
    respond_to do |format|
      format.html { redirect_to do_lists_url }
      format.json { head :no_content }
    end
  end


  def do_lists_publish_mark
    if params[:task_id]
      @output = DoList.where(:id => params[:task_id])
      if @output
        @output.update(:task_publish => true)
      end

      output_task_values = TaskValue.where(:do_lists_id => params[:task_id])
      data = "<h1>Task Name</h1><br/> <h4 style='color:green;font-size:22px'>"+@output.first.task+"</h4> <br/><h2>Task Details</b><br/><br/>"

      output_task_values.each do |x|
        if x.task_values_publish == true
          publish = "Mark"
        else
          publish = "Unmark"
        end
        data.concat("<div style='display: inline'><h4 style='color:green'>"+x.task_values+" => "+publish+"</h4></div>")
      end

      RestClient.post "https://api:key-07d67720afff84541f77fa15d0bc577a@api.mailgun.net/v3/sandboxa7b3b25f08344ca38d6628f09494ceb7.mailgun.org/messages", :from => "Mailgun Task List <postmaster@sandboxa7b3b25f08344ca38d6628f09494ceb7.mailgun.org>", :to => "Dev <dev.jangra031@gmail.com>", :subject => "Task List Done", :html => data
      redirect_to do_lists_url
    end
  end

  def do_lists_publish_unmark
    if params[:task_id]
      @output = DoList.where(:id => params[:task_id])
      if @output
        @output.update(:task_publish => false)
      end
      redirect_to do_lists_url
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_do_list
      @do_list = DoList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def do_list_params
      params.require(:do_list).permit(:task,:task_values)
    end
end
